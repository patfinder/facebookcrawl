﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FacebookCrawl
{
    public enum ServiceState
    {
        SERVICE_STOPPED = 0x00000001,
        SERVICE_START_PENDING = 0x00000002,
        SERVICE_STOP_PENDING = 0x00000003,
        SERVICE_RUNNING = 0x00000004,
        SERVICE_CONTINUE_PENDING = 0x00000005,
        SERVICE_PAUSE_PENDING = 0x00000006,
        SERVICE_PAUSED = 0x00000007,
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public int dwServiceType;
        public ServiceState dwCurrentState;
        public int dwControlsAccepted;
        public int dwWin32ExitCode;
        public int dwServiceSpecificExitCode;
        public int dwCheckPoint;
        public int dwWaitHint;
    };

    public partial class FBCrawlerService : ServiceBase
    {
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);

        public FBCrawlerService()
        {
            InitializeComponent();

            if (!System.Diagnostics.EventLog.SourceExists("FacebookCrawler"))
            {
                System.Diagnostics.EventLog.CreateEventSource("FacebookCrawler", "FBCrawler");
            }
            this.fbLogger.Source = "FacebookCrawler";
            this.fbLogger.Log = "FBCrawler";
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                fbLogger.WriteEntry("In OnStart.");
            }
            catch(Exception ex)
            {
                try
                {
                    File.WriteAllLines(@"d:\tmp\FBCrawler.log", new[] { ex.ToString() });
                }
                catch (Exception) { }
            }
        }

        protected override void OnStop()
        {
            try
            {
                fbLogger.WriteEntry("In OnStop.");
            }
            catch (Exception) { }
        }

        protected override void OnContinue()
        {
            fbLogger.WriteEntry("In OnContinue.");
        }
    }
}
