﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FacebookCrawl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // RUN ONE TIME ONLY
            //if (System.Diagnostics.EventLog.SourceExists("Service1"))
            //{
            //    System.Diagnostics.EventLog.DeleteEventSource("FacebookCrawler");
            //}
            // ~RUN ONE TIME ONLY~


            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new FBCrawlerService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
